# Model Definition
| Parameter | Description | Sample | PII | Sensitive | Unique Identifier | Mandatory | Default | Details |
| --- | --- | --- | --- | --- | --- | --- | --- | --- |
|  authorizedUser |  |  -  | No | No | No | No |  |Data Type : object<br>  |
| &gt; title |  |  -  | No | No | No | No |  |Data Type : string<br> Min. length :  - <br> Max. length :  - <br> Regex :  - <br> Allow Null : true<br>  |
| &gt; phoneNumber |  |  -  | No | No | No | No |  |Data Type : string<br> Min. length :  - <br> Max. length :  - <br> Regex :  - <br> Allow Null : true<br>  |
| &gt; password |  |  -  | No | No | No | No |  |Data Type : string<br> Min. length :  - <br> Max. length :  - <br> Regex :  - <br> Allow Null : true<br>  |
| &gt; authorizedUser |  | USER | No | No | No | No |  |Data Type : string<br> Min. length :  - <br> Max. length :  - <br> Regex :  - <br>  |
| &gt; authorizedName |  | STRICT AUTHORIZATION | No | No | No | No |  |Data Type : string<br> Min. length :  - <br> Max. length :  - <br> Regex :  - <br>  |





```
{
  "title": "",
  "phoneNumber": "",
  "password": "",
  "authorizedUser": "USER",
  "authorizedName": "STRICT AUTHORIZATION"
}
```




```
{
  "$schema": "http://json-schema.org/draft-04/schema#",
  "title": "authorizedUser",
  "type": "object",
  "properties": {
    "title": {
      "title": "title",
      "description": "",
      "type": "string",
      "pattern": ""
    },
    "phoneNumber": {
      "title": "phoneNumber",
      "description": "",
      "type": "string",
      "pattern": ""
    },
    "password": {
      "title": "password",
      "description": "",
      "type": "string",
      "pattern": ""
    },
    "authorizedUser": {
      "title": "authorizedUser",
      "type": "string"
    },
    "authorizedName": {
      "title": "authorizedName",
      "type": "string"
    }
  }
}
```

