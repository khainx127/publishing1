# Model Definition
| Parameter | Description | Sample | PII | Sensitive | Unique Identifier | Mandatory | Default | Details |
| --- | --- | --- | --- | --- | --- | --- | --- | --- |
|  Error |  |  -  | No | No | No | No |  |Data Type : object<br>  |
| &gt; Error field |  | filter.historyStartDate | No | No | No | No |  |Data Type : string<br> Min. length :  - <br> Max. length :  - <br> Regex :  - <br>  |
| &gt; Error message |  | Accepted Date format yyyy/mm/dd for history start date. | No | No | No | No |  |Data Type : string<br> Min. length :  - <br> Max. length :  - <br> Regex :  - <br>  |
| &gt; Error Code |  | D001 | No | No | No | No |  |Data Type : string<br> Min. length :  - <br> Max. length :  - <br> Regex :  - <br>  |





```
{
  "field": "filter.historyStartDate",
  "message": "Accepted Date format yyyy/mm/dd for history start date.",
  "code": "D001"
}
```




```
{
  "$schema": "http://json-schema.org/draft-04/schema#",
  "title": "Error",
  "description": "",
  "type": "object",
  "properties": {
    "field": {
      "title": "Error field",
      "type": "string"
    },
    "message": {
      "title": "Error message",
      "type": "string"
    },
    "code": {
      "title": "Error Code",
      "type": "string"
    }
  }
}
```

