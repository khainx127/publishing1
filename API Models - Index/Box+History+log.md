# Model Definition
| Parameter | Description | Sample | PII | Sensitive | Unique Identifier | Mandatory | Default | Details |
| --- | --- | --- | --- | --- | --- | --- | --- | --- |
|  Box History log | &#xA;&#xA;Box history log object defintion&#xA; |  -  | No | No | No | No |  |Data Type : object<br>  |
| &gt; Box Number | &#xA;&#xA;The unique Box Number assigned to the box by Iron Mountain when the box is received.&#xA; | 2 | No | No | **YES** | No |  |Data Type : string<br> Min. length :  - <br> Max. length :  - <br> Regex :  - <br> Allow Null : false<br> Faker : random.number<br>  |
| &gt; Barcode | &#xA;&#xA;The unique identifier of box which is generated from Iron Mountain.&#xA; | 000003763855802 | No | No | **YES** | No |  |Data Type : string<br> Min. length :  - <br> Max. length :  - <br> Regex :  - <br>  |
| &gt; Order Number |  | 111222 | No | No | No | No |  |Data Type : number<br> Minimum :  - <br> Exclusive Minimum : No<br> Maximum :  - <br> Exclusive Maximum : No<br> Multiple Of :  - <br>  |
| &gt; External OrderNumber |  | 7777 | No | No | No | No |  |Data Type : number<br> Minimum :  - <br> Exclusive Minimum : No<br> Maximum :  - <br> Exclusive Maximum : No<br> Multiple Of :  - <br>  |
| &gt; Order Line Number |  | 0 | No | No | No | No |  |Data Type : number<br> Minimum :  - <br> Exclusive Minimum : No<br> Maximum :  - <br> Exclusive Maximum : No<br> Multiple Of :  - <br>  |
| &gt; History Status Description |  | Received at Iron Mountain | No | No | No | No |  |Data Type : string<br> Min. length :  - <br> Max. length :  - <br> Regex :  - <br>  |
| &gt; History Date Time |  | 2014/01/22 01:24:33.000 | No | No | No | No |  |Data Type : string<br> Min. length :  - <br> Max. length :  - <br> Regex :  - <br>  |
| &gt; Requested By |  | JDOE | No | No | No | No |  |Data Type : string<br> Min. length :  - <br> Max. length :  - <br> Regex :  - <br>  |
| &gt; Requested For |  | JDOE | No | No | No | No |  |Data Type : string<br> Min. length :  - <br> Max. length :  - <br> Regex :  - <br>  |
| &gt; Order Contact |  | JDOE | No | No | No | No |  |Data Type : string<br> Min. length :  - <br> Max. length :  - <br> Regex :  - <br>  |
| &gt; Requesting Department |  |  -  | No | No | No | No |  |Data Type : string<br> Min. length :  - <br> Max. length :  - <br> Regex :  - <br>  |
| &gt; Requesting Division |  |  -  | No | No | No | No |  |Data Type : string<br> Min. length :  - <br> Max. length :  - <br> Regex :  - <br>  |
| &gt; History Log Id |  | 123456789 | No | No | No | No |  |Data Type : string<br> Min. length :  - <br> Max. length :  - <br> Regex :  - <br>  |
| &gt; Disposition |  |  -  | No | No | No | No |  |Data Type : string<br> Min. length :  - <br> Max. length :  - <br> Regex :  - <br>  |
| &gt; History Status |  | NEW | No | No | No | No |  |Data Type : string<br> Min. length :  - <br> Max. length :  - <br> Regex :  - <br>  |
| &gt; Transaction DateTime | &#xA;&#xA;The last modified date/time of the box.&#xA; | 2014/01/22T01:24:33.000 | No | No | No | No |  |Data Type : string<br> Min. length :  - <br> Max. length :  - <br> Regex : /^\d{4}/\d\d/\d\dT\d\d:\d\d:\d\d(\.\d&#x2B;)?(([&#x2B;-]\d\d:\d\d)|Z)?$/<br>  |





```
{
  "boxNumber": "2",
  "barcode": "000003763855802",
  "orderNumber": 111222.0,
  "externalOrderNumber": 7777.0,
  "orderLineNumber": 0.0,
  "statusDescription": "Received at Iron Mountain",
  "historyDateTime": "2014/01/22 01:24:33.000",
  "requestedBy": "JDOE",
  "requestedFor": "JDOE",
  "orderContact": "JDOE",
  "requestingDepartment": "",
  "requestingDivision": "",
  "id": "123456789",
  "disposition": "",
  "historyStatus": "NEW",
  "transactionDateTime": "2014/01/22T01:24:33.000"
}
```




```
{
  "$schema": "http://json-schema.org/draft-04/schema#",
  "title": "Box History log",
  "description": "<p>Box history log object defintion</p>",
  "type": "object",
  "properties": {
    "boxNumber": {
      "title": "Box Number",
      "description": "<p>The unique Box Number assigned to the box by Iron Mountain when the box is received.</p>",
      "type": "string",
      "pattern": ""
    },
    "barcode": {
      "title": "Barcode",
      "description": "<p>The unique identifier of box which is generated from Iron Mountain.</p>",
      "type": "string",
      "pattern": ""
    },
    "orderNumber": {
      "title": "Order Number",
      "type": "number"
    },
    "externalOrderNumber": {
      "title": "External OrderNumber",
      "type": "number"
    },
    "orderLineNumber": {
      "title": "Order Line Number",
      "type": "number"
    },
    "statusDescription": {
      "title": "History Status Description",
      "type": "string"
    },
    "historyDateTime": {
      "title": "History Date Time",
      "type": "string"
    },
    "requestedBy": {
      "title": "Requested By",
      "type": "string"
    },
    "requestedFor": {
      "title": "Requested For",
      "type": "string"
    },
    "orderContact": {
      "title": "Order Contact",
      "type": "string"
    },
    "requestingDepartment": {
      "title": "Requesting Department",
      "type": "string"
    },
    "requestingDivision": {
      "title": "Requesting Division",
      "type": "string"
    },
    "id": {
      "title": "History Log Id",
      "type": "string"
    },
    "disposition": {
      "title": "Disposition",
      "type": "string"
    },
    "historyStatus": {
      "title": "History Status",
      "type": "string"
    },
    "transactionDateTime": {
      "title": "Transaction DateTime",
      "description": "<p>The last modified date/time of the box.</p>",
      "type": "string",
      "pattern": "/^\\d{4}/\\d\\d/\\d\\dT\\d\\d:\\d\\d:\\d\\d(\\.\\d+)?(([+-]\\d\\d:\\d\\d)|Z)?$/"
    }
  }
}
```

