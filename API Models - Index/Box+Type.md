# Model Definition
| Parameter | Description | Sample | PII | Sensitive | Unique Identifier | Mandatory | Default | Details |
| --- | --- | --- | --- | --- | --- | --- | --- | --- |
|  Box Type |  |  -  | No | No | No | No |  |Data Type : object<br>  |
| &gt; Box Type |  | 1.2 | No | No | No | No |  |Data Type : string<br> Min. length :  - <br> Max. length :  - <br> Regex :  - <br>  |
| &gt; Box Description |  | Standard 1.2 cubft box | No | No | No | No |  |Data Type : string<br> Min. length :  - <br> Max. length :  - <br> Regex :  - <br>  |
| &gt; Box Cubic Foot |  | 1.2 | No | No | No | No |  |Data Type : number<br> Minimum :  - <br> Exclusive Minimum : No<br> Maximum :  - <br> Exclusive Maximum : No<br> Multiple Of :  - <br> Allow Null : false<br> Enum : 1.2, 2.4<br>  |





```
{
  "type": "1.2",
  "description": "Standard 1.2 cubft box",
  "cubicFoot": 1.2
}
```




```
{
  "$schema": "http://json-schema.org/draft-04/schema#",
  "title": "Box Type",
  "description": "",
  "type": "object",
  "properties": {
    "type": {
      "title": "Box Type",
      "type": "string"
    },
    "description": {
      "title": "Box Description",
      "type": "string"
    },
    "cubicFoot": {
      "title": "Box Cubic Foot",
      "description": "",
      "type": "number",
      "enum": [
        "1.2",
        "2.4"
      ]
    }
  }
}
```

