# Model Definition
| Parameter | Description | Sample | PII | Sensitive | Unique Identifier | Mandatory | Default | Details |
| --- | --- | --- | --- | --- | --- | --- | --- | --- |
|  Address | &#xA;&#xA;Address object&#xA; |  -  | No | No | No | No |  |Data Type : object<br>  |
| &gt; Address Type |  | CORRESPONDENCE | No | No | No | No |  |Data Type : string<br> Min. length :  - <br> Max. length :  - <br> Regex :  - <br>  |
| &gt; Customer Name |  | Test Customer | No | No | No | No |  |Data Type : string<br> Min. length :  - <br> Max. length :  - <br> Regex :  - <br>  |
| &gt; Address1 |  | 3333 PEACHTREE ROAD | No | No | No | No |  |Data Type : string<br> Min. length :  - <br> Max. length :  - <br> Regex :  - <br>  |
| &gt; Address2 |  |  -  | No | No | No | No |  |Data Type : string<br> Min. length :  - <br> Max. length :  - <br> Regex :  - <br> Allow Null : true<br>  |
| &gt; Address3 |  |  -  | No | No | No | No |  |Data Type : string<br> Min. length :  - <br> Max. length :  - <br> Regex :  - <br> Allow Null : true<br>  |
| &gt; Floor |  | 9E | No | No | No | No |  |Data Type : string<br> Min. length :  - <br> Max. length :  - <br> Regex :  - <br>  |
| &gt; City |  | EAGLEVILLE | No | No | No | No |  |Data Type : string<br> Min. length :  - <br> Max. length :  - <br> Regex :  - <br>  |
| &gt; State |  | PA | No | No | No | No |  |Data Type : string<br> Min. length :  - <br> Max. length :  - <br> Regex :  - <br>  |
| &gt; Country |  | USA | No | No | No | No |  |Data Type : string<br> Min. length :  - <br> Max. length :  - <br> Regex :  - <br>  |
| &gt; Zip |  | 19403 | No | No | No | No |  |Data Type : string<br> Min. length :  - <br> Max. length :  - <br> Regex :  - <br>  |
| &gt; Contact Name |  | JULIE MADDOX | No | No | No | No |  |Data Type : string<br> Min. length :  - <br> Max. length :  - <br> Regex :  - <br>  |
| &gt; phoneNumber |  |  -  | No | No | No | No |  |Data Type : string<br> Min. length :  - <br> Max. length :  - <br> Regex :  - <br> Allow Null : true<br>  |
| &gt; Extension Number |  | 0124 | No | No | No | No |  |Data Type : string<br> Min. length :  - <br> Max. length :  - <br> Regex :  - <br>  |
| &gt; Fax Number |  | 9112435654 | No | No | No | No |  |Data Type : string<br> Min. length :  - <br> Max. length :  - <br> Regex :  - <br>  |
| &gt; Email |  | guna@gmail.com | No | No | No | No |  |Data Type : string<br> Min. length :  - <br> Max. length :  - <br> Regex :  - <br>  |





```
{
  "addressType": "CORRESPONDENCE",
  "customerName": "Test Customer",
  "address1": "3333 PEACHTREE ROAD",
  "address2": "",
  "address3": "",
  "floor": "9E",
  "city": "EAGLEVILLE",
  "state": "PA",
  "country": "USA",
  "zip": "19403",
  "contactName": "JULIE MADDOX",
  "phoneNumber": "",
  "extensionNo": "0124",
  "faxNumber": "9112435654",
  "email": "guna@gmail.com"
}
```




```
{
  "$schema": "http://json-schema.org/draft-04/schema#",
  "title": "Address",
  "description": "<p>Address object</p>",
  "type": "object",
  "properties": {
    "addressType": {
      "title": "Address Type",
      "type": "string"
    },
    "customerName": {
      "title": "Customer Name",
      "type": "string"
    },
    "address1": {
      "title": "Address1",
      "type": "string"
    },
    "address2": {
      "title": "Address2",
      "type": "string",
      "pattern": ""
    },
    "address3": {
      "title": "Address3",
      "type": "string",
      "pattern": ""
    },
    "floor": {
      "title": "Floor",
      "type": "string"
    },
    "city": {
      "title": "City",
      "type": "string"
    },
    "state": {
      "title": "State",
      "type": "string"
    },
    "country": {
      "title": "Country",
      "type": "string"
    },
    "zip": {
      "title": "Zip",
      "type": "string"
    },
    "contactName": {
      "title": "Contact Name",
      "type": "string"
    },
    "phoneNumber": {
      "title": "phoneNumber",
      "description": "",
      "type": "string",
      "pattern": ""
    },
    "extensionNo": {
      "title": "Extension Number",
      "type": "string"
    },
    "faxNumber": {
      "title": "Fax Number",
      "type": "string"
    },
    "email": {
      "title": "Email",
      "type": "string"
    }
  },
  "required": [
    "addressType",
    "customerName",
    "city",
    "state",
    "country",
    "zip"
  ]
}
```

