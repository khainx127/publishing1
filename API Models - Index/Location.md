# Model Definition
| Parameter | Description | Sample | PII | Sensitive | Unique Identifier | Mandatory | Default | Details |
| --- | --- | --- | --- | --- | --- | --- | --- | --- |
|  Location |  |  -  | No | No | No | No |  |Data Type : object<br>  |
| &gt; location Id | &#xA;&#xA;Location Id of Iron mountain box.&#xA; | SDFL02432003430403 | No | No | No | No |  |Data Type : string<br> Min. length :  - <br> Max. length :  - <br> Regex :  - <br> Allow Null : true<br>  |
| &gt; Location Description | &#xA;&#xA;Location description of the Iron Mountain facility.&#xA; | SAN DIEGO, CA | No | No | No | No |  |Data Type : string<br> Min. length :  - <br> Max. length :  - <br> Regex :  - <br> Allow Null : true<br>  |





```
{
  "locationId": "SDFL02432003430403",
  "locationDescription": "SAN DIEGO, CA"
}
```




```
{
  "$schema": "http://json-schema.org/draft-04/schema#",
  "title": "Location",
  "type": "object",
  "properties": {
    "locationId": {
      "title": "location Id",
      "description": "<p>Location Id of Iron mountain box.</p>",
      "type": "string",
      "pattern": ""
    },
    "locationDescription": {
      "title": "Location Description",
      "description": "<p>Location description of the Iron Mountain facility.</p>",
      "type": "string",
      "pattern": ""
    }
  }
}
```

