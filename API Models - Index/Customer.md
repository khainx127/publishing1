# Model Definition
| Parameter | Description | Sample | PII | Sensitive | Unique Identifier | Mandatory | Default | Details |
| --- | --- | --- | --- | --- | --- | --- | --- | --- |
|  Customer |  |  -  | No | No | No | No |  |Data Type : object<br>  |
| &gt; Customer Id |  | 10 | No | No | No | No |  |Data Type : integer<br> Minimum :  - <br> Exclusive Minimum : No<br> Maximum :  - <br> Exclusive Maximum : No<br> Multiple Of :  - <br>  |
| &gt; Customer name |  | abc | No | No | No | No |  |Data Type : string<br> Min. length :  - <br> Max. length :  - <br> Regex :  - <br>  |





```
{
  "customerId": 10,
  "customerName": "abc"
}
```




```
{
  "$schema": "http://json-schema.org/draft-04/schema#",
  "title": "Customer",
  "type": "object",
  "properties": {
    "customerId": {
      "title": "Customer Id",
      "type": "integer"
    },
    "customerName": {
      "title": "Customer name",
      "type": "string"
    }
  }
}
```

