# Model Definition
| Parameter | Description | Sample | PII | Sensitive | Unique Identifier | Mandatory | Default | Details |
| --- | --- | --- | --- | --- | --- | --- | --- | --- |
|  File | &#xA;&#xA;Contains all details of the file record&#xA; |  -  | No | No | No | No |  |Data Type : object<br>  |
| &gt; Customer Id | &#xA;&#xA;The customer ID of the customer the box belongs to.&#xA; | X999 | No | No | No | No |  |Data Type : string<br> Min. length :  - <br> Max. length :  - <br> Regex :  - <br>  |
| &gt; Division Id | &#xA;&#xA;The division ID of the box belongs to.&#xA; |  -  | No | No | No | No |  |Data Type : string<br> Min. length :  - <br> Max. length :  - <br> Regex :  - <br> Allow Null : true<br>  |
| &gt; Department Id | &#xA;&#xA;Department id belongs to the division selected, else department is null.&#xA; |  -  | No | No | No | No |  |Data Type : string<br> Min. length :  - <br> Max. length :  - <br> Regex :  - <br> Allow Null : true<br>  |
| &gt; Box Number | &#xA;&#xA;The unique Box Number assigned to the box by Iron Mountain when the box is received.&#xA; | 2 | No | No | **YES** | No |  |Data Type : string<br> Min. length :  - <br> Max. length :  - <br> Regex :  - <br>  |
| &gt; File Sequence Number |  | 1 | No | No | No | No |  |Data Type : number<br> Minimum :  - <br> Exclusive Minimum : No<br> Maximum :  - <br> Exclusive Maximum : No<br> Multiple Of :  - <br>  |
| &gt; Barcode | &#xA;&#xA;The unique identifier of box which is generated from Iron Mountain.&#xA; | 000003763855802 | No | No | **YES** | No |  |Data Type : string<br> Min. length :  - <br> Max. length :  - <br> Regex :  - <br>  |
| &gt; Customer Barcode |  |  -  | No | No | No | No |  |Data Type : string<br> Min. length :  - <br> Max. length :  - <br> Regex :  - <br>  |
| &gt; status | &#xA;&#xA;|-&#xA;&#xA;&#xA; Current status of the Box.&#xA;&#xA;&#xA; | Code | Description | Comments |&#xA;&#xA;&#xA; | ----------- | ----------- | ----------- | &#xA;&#xA;&#xA; | DSO | DESTROYED OUTCARDED | The box was on a destruction order, but that box was not in Iron Mountain possession at the time. The box was out to the customer. |&#xA;&#xA;&#xA; |DST |DESTROYED |The item was on a destruction order and processed.|&#xA;&#xA;&#xA; |NRM|NORMAL |Indicates that the box was scanned to the shelf.|&#xA;&#xA;&#xA; |OUT|OUTCARDED|Indicates that the box was released and delivered to a customer(expecting the item back).|&#xA;&#xA;&#xA; |PUT|PUTAWAY|Indicates that the box is in Iron Mountain possession but not in its assigned location.|&#xA;&#xA;&#xA; |RSH|RESEARCH|Indicates that the box isn&#x27;t found when it should have been on the shelf.|&#xA;&#xA;&#xA; |WTH|WITHDRAWN|Indicates that the box was released and delivered to a customer(not expecting the item back).|&#xA; | NRM | No | No | No | No | NRM |Data Type : string<br> Min. length :  - <br> Max. length :  - <br> Regex :  - <br> Allow Null : false<br> Enum : DSO, DST, NRM, OUT, PUT, RSH, WTH<br>  |
| &gt; Transaction DateTime | &#xA;&#xA;The last modified date/time of the box.&#xA; | 2014/01/22T01:24:33.000 | No | No | No | No |  |Data Type : string<br> Min. length :  - <br> Max. length :  - <br> Regex : /^\d{4}/\d\d/\d\dT\d\d:\d\d:\d\d(\.\d&#x2B;)?(([&#x2B;-]\d\d:\d\d)|Z)?$/<br>  |
| &gt; Transaction UserId | &#xA;&#xA;The user who did the last modification to the box.&#xA; | bob | No | No | No | No |  |Data Type : string<br> Min. length :  - <br> Max. length :  - <br> Regex :  - <br>  |
| &gt; Image Enabled |  | False | No | No | No | No |  |Data Type : boolean<br>  |
| &gt; Temporary File Enabled |  | False | No | No | No | No |  |Data Type : boolean<br>  |
| &gt; Create Date | &#xA;&#xA;The create date of the box.The format of the value is [yyyy/mm/dd].&#xA; | 2011/05/17 | No | No | No | No |  |Data Type : string<br> Min. length :  - <br> Max. length :  - <br> Regex : /([12]\d{3}/(0[1-9]|1[0-2])/(0[1-9]|[12]\d|3[01]))/<br>  |
| &gt; Birth Date |  | 2001/11/02 | No | No | No | No |  |Data Type : string<br> Min. length :  - <br> Max. length :  - <br> Regex :  - <br>  |
| &gt; Discharge Date |  | 2001/11/02 | No | No | No | No |  |Data Type : string<br> Min. length :  - <br> Max. length :  - <br> Regex :  - <br>  |
| &gt; From Date | &#xA;&#xA;Boxes/Files contents belongs to this from date of the customer. The format of the value is [yyyy/mm/dd].&#xA; | 2011/01/31 | No | No | No | No |  |Data Type : string<br> Min. length :  - <br> Max. length :  - <br> Regex : /([12]\d{3}/(0[1-9]|1[0-2])/(0[1-9]|[12]\d|3[01]))/<br> Allow Null : true<br>  |
| &gt; To Date | &#xA;&#xA;Boxes/Files contents belongs to this To date of the customer.The format of the value is [yyyy/mm/dd].&#xA; | 2020/01/31 | No | No | No | No |  |Data Type : string<br> Min. length :  - <br> Max. length :  - <br> Regex : /([12]\d{3}/(0[1-9]|1[0-2])/(0[1-9]|[12]\d|3[01]))/<br>  |
| &gt; Alpha From | &#xA;&#xA;The alpha from value given for the box.&#xA; |  -  | No | No | No | No |  |Data Type : string<br> Min. length :  - <br> Max. length :  - <br> Regex :  - <br> Allow Null : true<br>  |
| &gt; Alpha To | &#xA;&#xA;The alpha to value given for the box.&#xA; |  -  | No | No | No | No |  |Data Type : string<br> Min. length :  - <br> Max. length :  - <br> Regex :  - <br> Allow Null : true<br>  |
| &gt; Volume Number |  | 0 | No | No | No | No |  |Data Type : number<br> Minimum :  - <br> Exclusive Minimum : No<br> Maximum :  - <br> Exclusive Maximum : No<br> Multiple Of :  - <br>  |
| &gt; Social Security Number |  |  -  | No | No | No | No |  |Data Type : string<br> Min. length :  - <br> Max. length :  - <br> Regex :  - <br>  |
| &gt; Major Description | &#xA;&#xA;Major description about the box contents.&#xA; | RMI TEST BOX | No | No | No | No |  |Data Type : string<br> Min. length :  - <br> Max. length :  - <br> Regex :  - <br> Allow Null : true<br>  |
| &gt; Minor Description | &#xA;&#xA;Minor description about the box contents.&#xA; |  -  | No | No | No | No |  |Data Type : string<br> Min. length :  - <br> Max. length :  - <br> Regex :  - <br> Allow Null : true<br>  |
| &gt; Description3 |  | WATER | No | No | No | No |  |Data Type : string<br> Min. length :  - <br> Max. length :  - <br> Regex :  - <br>  |
| &gt; Description4 |  | SODA | No | No | No | No |  |Data Type : string<br> Min. length :  - <br> Max. length :  - <br> Regex :  - <br>  |
| &gt; Description5 |  |  -  | No | No | No | No |  |Data Type : string<br> Min. length :  - <br> Max. length :  - <br> Regex :  - <br>  |
| &gt; Description6 |  |  -  | No | No | No | No |  |Data Type : string<br> Min. length :  - <br> Max. length :  - <br> Regex :  - <br>  |
| &gt; Description7 |  |  -  | No | No | No | No |  |Data Type : string<br> Min. length :  - <br> Max. length :  - <br> Regex :  - <br>  |
| &gt; Description8 |  |  -  | No | No | No | No |  |Data Type : string<br> Min. length :  - <br> Max. length :  - <br> Regex :  - <br>  |
| &gt; Description9 |  |  -  | No | No | No | No |  |Data Type : string<br> Min. length :  - <br> Max. length :  - <br> Regex :  - <br>  |
| &gt; Destruction Date | &#xA;&#xA;The destruction date of the box. The format of the value is [yyyy/mm/dd].&#xA; | 2011/08/17 | No | No | No | No |  |Data Type : string<br> Min. length :  - <br> Max. length :  - <br> Regex : /([12]\d{3}/(0[1-9]|1[0-2])/(0[1-9]|[12]\d|3[01]))/<br> Allow Null : true<br>  |
| &gt; Box Enabled |  | True | No | No | No | No |  |Data Type : boolean<br>  |
| &gt; Receipt Date | &#xA;&#xA;The receipt date of the box.The format of the value is [yyyy/mm/dd].&#xA; | 2012/06/19 | No | No | No | No |  |Data Type : string<br> Min. length :  - <br> Max. length :  - <br> Regex : /([12]\d{3}/(0[1-9]|1[0-2])/(0[1-9]|[12]\d|3[01]))/<br>  |
| &gt; location Id | &#xA;&#xA;Location Id of Iron mountain box.&#xA; | SDFL02432003430403 | No | No | No | No |  |Data Type : string<br> Min. length :  - <br> Max. length :  - <br> Regex :  - <br> Allow Null : true<br>  |
| &gt; Asset Type |  |  -  | No | No | No | No |  |Data Type : string<br> Min. length :  - <br> Max. length :  - <br> Regex :  - <br>  |
| &gt; File Unique Barcode |  |  -  | No | No | No | No |  |Data Type : string<br> Min. length :  - <br> Max. length :  - <br> Regex :  - <br>  |
| &gt; Record Code |  |  -  | No | No | No | No |  |Data Type : string<br> Min. length :  - <br> Max. length :  - <br> Regex :  - <br>  |
| &gt; ThirdParty Id |  |  -  | No | No | No | No |  |Data Type : string<br> Min. length :  - <br> Max. length :  - <br> Regex :  - <br>  |
| &gt; Customer Box Number | &#xA;&#xA;The box number defined/used by the customer.&#xA; | 2 | No | No | No | No |  |Data Type : string<br> Min. length :  - <br> Max. length :  - <br> Regex :  - <br>  |
| &gt; Location Description | &#xA;&#xA;Location description of the Iron Mountain facility.&#xA; | SAN DIEGO, CA | No | No | No | No |  |Data Type : string<br> Min. length :  - <br> Max. length :  - <br> Regex :  - <br> Allow Null : true<br>  |
| &gt; Long Description | &#xA;&#xA;Long description about the Iron Mountain box.&#xA; | QWE QWE WERWE | No | No | No | No |  |Data Type : string<br> Min. length :  - <br> Max. length :  - <br> Regex :  - <br> Allow Null : true<br>  |
| &gt; Order Number |  | 111222 | No | No | No | No |  |Data Type : number<br> Minimum :  - <br> Exclusive Minimum : No<br> Maximum :  - <br> Exclusive Maximum : No<br> Multiple Of :  - <br>  |
| &gt; Order Type |  | 2 | No | No | No | No |  |Data Type : string<br> Min. length :  - <br> Max. length :  - <br> Regex :  - <br>  |
| &gt; Hold Code |  | HLD | No | No | No | No |  |Data Type : string<br> Min. length :  - <br> Max. length :  - <br> Regex :  - <br>  |





```
{
  "customerId": "X999",
  "divisionId": "",
  "departmentId": "",
  "boxNumber": "2",
  "fileSequenceNumber": 1.0,
  "barcode": "000003763855802",
  "customerBarcode": "",
  "status": "DSO",
  "transactionDateTime": "2014/01/22T01:24:33.000",
  "transactionUserId": "bob",
  "imageEnabled": false,
  "temporaryFileEnabled": false,
  "createDate": "2011/05/17",
  "birthDate": "2001/11/02",
  "dischargeDate": "2001/11/02",
  "fromDate": "2011/01/31",
  "toDate": "2020/01/31",
  "alphaFrom": "",
  "alphaTo": "",
  "volumeNumber": 0.0,
  "socialSecurityNumber": "",
  "majorDescription": "RMI TEST BOX",
  "minorDescription": "",
  "description3": "WATER",
  "description4": "SODA",
  "description5": "",
  "description6": "",
  "description7": "",
  "description8": "",
  "description9": "",
  "destructionDate": "2011/08/17",
  "boxEnabled": false,
  "receiptDate": "2012/06/19",
  "locationId": "SDFL02432003430403",
  "assetType": "",
  "fileUniqueBarcode": "",
  "recordCode": "",
  "thirdPartyId": "",
  "customerBoxNumber": "2",
  "locationDescription": "SAN DIEGO, CA",
  "longDescription": "QWE QWE WERWE",
  "orderNumber": 111222.0,
  "orderType": "2",
  "holdCode": "HLD"
}
```




```
{
  "$schema": "http://json-schema.org/draft-04/schema#",
  "title": "File",
  "description": "<p>Contains all details of the file record</p>",
  "type": "object",
  "properties": {
    "customerId": {
      "title": "Customer Id",
      "description": "<p>The customer ID of the customer the box belongs to.</p>",
      "type": "string",
      "pattern": ""
    },
    "divisionId": {
      "title": "Division Id",
      "description": "<p>The division ID of the box belongs to.</p>",
      "type": "string",
      "pattern": ""
    },
    "departmentId": {
      "title": "Department Id",
      "description": "<p>Department id belongs to the division selected, else department is null.</p>",
      "type": "string",
      "pattern": ""
    },
    "boxNumber": {
      "title": "Box Number",
      "description": "<p>The unique Box Number assigned to the box by Iron Mountain when the box is received.</p>",
      "type": "string",
      "pattern": ""
    },
    "fileSequenceNumber": {
      "title": "File Sequence Number",
      "type": "number"
    },
    "barcode": {
      "title": "Barcode",
      "description": "<p>The unique identifier of box which is generated from Iron Mountain.</p>",
      "type": "string",
      "pattern": ""
    },
    "customerBarcode": {
      "title": "Customer Barcode",
      "type": "string"
    },
    "status": {
      "title": "status",
      "description": "<p>|-</p><p>            Current status of the Box.</p><p>            | Code      | Description | Comments |</p><p>            | ----------- | ----------- | ----------- | </p><p>            | DSO      | DESTROYED OUTCARDED       | The box was on a destruction order, but that box was not in Iron Mountain possession at the time. The box was out to the customer. |</p><p>            |DST |DESTROYED |The item was on a destruction order and processed.|</p><p>            |NRM|NORMAL |Indicates that the box was scanned to the shelf.|</p><p>            |OUT|OUTCARDED|Indicates that the box was released and delivered to a customer(expecting the item back).|</p><p>            |PUT|PUTAWAY|Indicates that the box is in Iron Mountain possession but not in its assigned location.|</p><p>            |RSH|RESEARCH|Indicates that the box isn't found when it should have been on the shelf.|</p><p>            |WTH|WITHDRAWN|Indicates that the box was released and delivered to a customer(not expecting the item back).|</p>",
      "type": "string",
      "default": "NRM",
      "pattern": "",
      "enum": [
        "DSO",
        "DST",
        "NRM",
        "OUT",
        "PUT",
        "RSH",
        "WTH"
      ]
    },
    "transactionDateTime": {
      "title": "Transaction DateTime",
      "description": "<p>The last modified date/time of the box.</p>",
      "type": "string",
      "pattern": "/^\\d{4}/\\d\\d/\\d\\dT\\d\\d:\\d\\d:\\d\\d(\\.\\d+)?(([+-]\\d\\d:\\d\\d)|Z)?$/"
    },
    "transactionUserId": {
      "title": "Transaction UserId",
      "description": "<p>The user who did the last modification to the box.</p>",
      "type": "string",
      "pattern": ""
    },
    "imageEnabled": {
      "title": "Image Enabled",
      "type": "boolean"
    },
    "temporaryFileEnabled": {
      "title": "Temporary File Enabled",
      "type": "boolean"
    },
    "createDate": {
      "title": "Create Date",
      "description": "<p>The create date of the box.The format of the value is [yyyy/mm/dd].</p>",
      "type": "string",
      "pattern": "/([12]\\d{3}/(0[1-9]|1[0-2])/(0[1-9]|[12]\\d|3[01]))/"
    },
    "birthDate": {
      "title": "Birth Date",
      "type": "string"
    },
    "dischargeDate": {
      "title": "Discharge Date",
      "type": "string"
    },
    "fromDate": {
      "title": "From Date",
      "description": "<p>Boxes/Files contents belongs to this from date of the customer. The format of the value is [yyyy/mm/dd].</p>",
      "type": "string",
      "pattern": "/([12]\\d{3}/(0[1-9]|1[0-2])/(0[1-9]|[12]\\d|3[01]))/"
    },
    "toDate": {
      "title": "To Date",
      "description": "<p>Boxes/Files contents belongs to this To date of the customer.The format of the value is [yyyy/mm/dd].</p>",
      "type": "string",
      "pattern": "/([12]\\d{3}/(0[1-9]|1[0-2])/(0[1-9]|[12]\\d|3[01]))/"
    },
    "alphaFrom": {
      "title": "Alpha From",
      "description": "<p>The alpha from value given for the box.</p>",
      "type": "string",
      "pattern": ""
    },
    "alphaTo": {
      "title": "Alpha To",
      "description": "<p>The alpha to value given for the box.</p>",
      "type": "string",
      "pattern": ""
    },
    "volumeNumber": {
      "title": "Volume Number",
      "type": "number"
    },
    "socialSecurityNumber": {
      "title": "Social Security Number",
      "type": "string"
    },
    "majorDescription": {
      "title": "Major Description",
      "description": "<p>Major description about the box contents.</p>",
      "type": "string",
      "pattern": ""
    },
    "minorDescription": {
      "title": "Minor Description",
      "description": "<p>Minor description about the box contents.</p>",
      "type": "string",
      "pattern": ""
    },
    "description3": {
      "title": "Description3",
      "type": "string"
    },
    "description4": {
      "title": "Description4",
      "type": "string"
    },
    "description5": {
      "title": "Description5",
      "type": "string"
    },
    "description6": {
      "title": "Description6",
      "type": "string"
    },
    "description7": {
      "title": "Description7",
      "type": "string"
    },
    "description8": {
      "title": "Description8",
      "type": "string"
    },
    "description9": {
      "title": "Description9",
      "type": "string"
    },
    "destructionDate": {
      "title": "Destruction Date",
      "description": "<p>The destruction date of the box. The format of the value is [yyyy/mm/dd].</p>",
      "type": "string",
      "pattern": "/([12]\\d{3}/(0[1-9]|1[0-2])/(0[1-9]|[12]\\d|3[01]))/"
    },
    "boxEnabled": {
      "title": "Box Enabled",
      "type": "boolean"
    },
    "receiptDate": {
      "title": "Receipt Date",
      "description": "<p>The receipt date of the box.The format of the value is [yyyy/mm/dd].</p>",
      "type": "string",
      "pattern": "/([12]\\d{3}/(0[1-9]|1[0-2])/(0[1-9]|[12]\\d|3[01]))/"
    },
    "locationId": {
      "title": "location Id",
      "description": "<p>Location Id of Iron mountain box.</p>",
      "type": "string",
      "pattern": ""
    },
    "assetType": {
      "title": "Asset Type",
      "type": "string"
    },
    "fileUniqueBarcode": {
      "title": "File Unique Barcode",
      "type": "string"
    },
    "recordCode": {
      "title": "Record Code",
      "type": "string"
    },
    "thirdPartyId": {
      "title": "ThirdParty Id",
      "type": "string"
    },
    "customerBoxNumber": {
      "title": "Customer Box Number",
      "description": "<p>The box number defined/used by the customer.</p>",
      "type": "string",
      "pattern": ""
    },
    "locationDescription": {
      "title": "Location Description",
      "description": "<p>Location description of the Iron Mountain facility.</p>",
      "type": "string",
      "pattern": ""
    },
    "longDescription": {
      "title": "Long Description",
      "description": "<p>Long description about the Iron Mountain box.</p>",
      "type": "string",
      "pattern": ""
    },
    "orderNumber": {
      "title": "Order Number",
      "type": "number"
    },
    "orderType": {
      "title": "Order Type",
      "type": "string"
    },
    "holdCode": {
      "title": "Hold Code",
      "type": "string"
    }
  }
}
```

