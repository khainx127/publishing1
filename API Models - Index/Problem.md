# Model Definition
| Parameter | Description | Sample | PII | Sensitive | Unique Identifier | Mandatory | Default | Details |
| --- | --- | --- | --- | --- | --- | --- | --- | --- |
|  Problem | &#xA;&#xA;Problem JSON Definition&#xA; |  -  | No | No | No | No |  |Data Type : object<br>  |
| &gt; Problem Type |  | https://ironmountain.github.io/problem/validation | No | No | No | No |  |Data Type : string<br> Min. length :  - <br> Max. length :  - <br> Regex :  - <br>  |
| &gt; Problem Detail |  | Box histories not availble for the given time period. | No | No | No | No |  |Data Type : string<br> Min. length :  - <br> Max. length :  - <br> Regex :  - <br>  |
| &gt; Problem Instance |  | /box/1234/box-histories | No | No | No | No |  |Data Type : string<br> Min. length :  - <br> Max. length :  - <br> Regex :  - <br>  |
| &gt; errors |  |  -  | No | No | No | No |  |Data Type : array<br>  |
| &gt; Error |  |  -  | No | No | No | No |  |Data Type : object<br>  |
| &gt; Error field |  | filter.historyStartDate | No | No | No | No |  |Data Type : string<br> Min. length :  - <br> Max. length :  - <br> Regex :  - <br>  |
| &gt; Error message |  | Accepted Date format yyyy/mm/dd for history start date. | No | No | No | No |  |Data Type : string<br> Min. length :  - <br> Max. length :  - <br> Regex :  - <br>  |
| &gt; Error Code |  | D001 | No | No | No | No |  |Data Type : string<br> Min. length :  - <br> Max. length :  - <br> Regex :  - <br>  |
| &gt; Error |  |  -  | No | No | No | No |  |Data Type : object<br>  |
| &gt; Error field |  | filter.historyStartDate | No | No | No | No |  |Data Type : string<br> Min. length :  - <br> Max. length :  - <br> Regex :  - <br>  |
| &gt; Error message |  | Accepted Date format yyyy/mm/dd for history start date. | No | No | No | No |  |Data Type : string<br> Min. length :  - <br> Max. length :  - <br> Regex :  - <br>  |
| &gt; Error Code |  | D001 | No | No | No | No |  |Data Type : string<br> Min. length :  - <br> Max. length :  - <br> Regex :  - <br>  |
| &gt; Error |  |  -  | No | No | No | No |  |Data Type : object<br>  |
| &gt; Error field |  | filter.historyStartDate | No | No | No | No |  |Data Type : string<br> Min. length :  - <br> Max. length :  - <br> Regex :  - <br>  |
| &gt; Error message |  | Accepted Date format yyyy/mm/dd for history start date. | No | No | No | No |  |Data Type : string<br> Min. length :  - <br> Max. length :  - <br> Regex :  - <br>  |
| &gt; Error Code |  | D001 | No | No | No | No |  |Data Type : string<br> Min. length :  - <br> Max. length :  - <br> Regex :  - <br>  |
| &gt; Problem Title |  | Bad Request | No | No | No | No |  |Data Type : string<br> Min. length :  - <br> Max. length :  - <br> Regex :  - <br>  |
| &gt; Problem HTTP Status |  | 400 | No | No | No | No |  |Data Type : number<br> Minimum :  - <br> Exclusive Minimum : No<br> Maximum :  - <br> Exclusive Maximum : No<br> Multiple Of :  - <br> Allow Null : false<br> Enum : 400, 401, 403, 404, 405, 415<br>  |





```
{
  "type": "https://ironmountain.github.io/problem/validation",
  "detail": "Box histories not availble for the given time period.",
  "instance": "/box/1234/box-histories",
  "errors": [
    {
      "field": "filter.historyStartDate",
      "message": "Accepted Date format yyyy/mm/dd for history start date.",
      "code": "D001"
    },
    {
      "field": "filter.historyStartDate",
      "message": "Accepted Date format yyyy/mm/dd for history start date.",
      "code": "D001"
    },
    {
      "field": "filter.historyStartDate",
      "message": "Accepted Date format yyyy/mm/dd for history start date.",
      "code": "D001"
    }
  ],
  "title": "Bad Request",
  "status": 400.0
}
```




```
{
  "$schema": "http://json-schema.org/draft-04/schema#",
  "title": "Problem",
  "description": "<p>Problem JSON Definition</p>",
  "type": "object",
  "properties": {
    "type": {
      "title": "Problem Type",
      "type": "string"
    },
    "detail": {
      "title": "Problem Detail",
      "type": "string"
    },
    "instance": {
      "title": "Problem Instance",
      "type": "string"
    },
    "errors": {
      "title": "errors",
      "type": "array",
      "items": {
        "title": "Error",
        "description": "",
        "type": "object",
        "properties": {
          "field": {
            "title": "Error field",
            "type": "string"
          },
          "message": {
            "title": "Error message",
            "type": "string"
          },
          "code": {
            "title": "Error Code",
            "type": "string"
          }
        }
      }
    },
    "title": {
      "title": "Problem Title",
      "type": "string"
    },
    "status": {
      "title": "Problem HTTP Status",
      "description": "",
      "type": "number",
      "enum": [
        "400",
        "401",
        "403",
        "404",
        "405",
        "415"
      ]
    }
  }
}
```

