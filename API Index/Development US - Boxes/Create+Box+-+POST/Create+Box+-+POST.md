# Create Box - POST

POST /v1/customers/{customerId}/boxes

## Description

&#xA;&#xA;Allows creation of box entity.&#xA;

* [Request Payloads](#request-payloads)
* [Response Payloads](#response-payloads)

| HTTP Method                           | Post|
| ------------------------------------- | ----------------------------------------------- |
| API                                   | Boxes                                           |
| Api Version                           | 1.0.0.102                                         |
| Resource Version                      | 1                                               |
| Summary                               |                                       |
| Base Path                             | /v1/customers/{customerId}/boxes                                     |
| Resource                              | Create Box                                      |
| Endpoint URL                          | https://wsg-dev-use.apigwy-np.eapg.ironmountain.cloud/v1/customers/{customerId}/boxes              |
| Service Status                        |  -                                          |
| Legislative / Regulatory / Compliance |                                             |
| Firewalls Details                     |                                              |
| Security Certificate Details          |                                              |
| Vendor or Partner Considerations      |                                             |

## Request Payloads

### Request Header



| Header | Description | Sample | PII | Sensitive | Unique Identifier | Mandatory | Default | Details |
| ------ | :---------: | :----: | :-: | :-------: | :---------------: | :-------: | :-----: | ------- |
| Api Header |  |  -  | No | No | No | No |  -  | Data Type : object<br>  |


---

### Query Params



| Parameter | Description | Sample | PII | Sensitive | Unique Identifier | Mandatory | Default | Details |
| --------- | :---------: | :----: | :-: | :-------: | :---------------: | :-------: | :-----: | ------- |
| Api Query Parameter |  |  -  | No | No | No | No |  -  | Data Type : object<br>  |


---

### Request Body

#### Payload 



| Parameter | Description | Sample | PII | Sensitive | Unique Identifier | Mandatory | Default | Details |
| :----- | :-----: | :-----: | :-----: | :-----: | :-----: | :-----: | :-----: | :----- |
| Box | &#xA;&#xA;Contains all details of the box record.&#xA; |  -  | No | No | No | No |  -  | Data Type : object<br>  |
| >Customer Box Number | &#xA;&#xA;The box number defined/used by the customer.&#xA; | 2 | No | No | No | No |  -  | Data Type : string<br> Min. length :  - <br> Max. length :  - <br> Regex :  - <br>  |
| >Customer Id | &#xA;&#xA;The customer ID of the customer the box belongs to.&#xA; | X999 | No | No | No | No |  -  | Data Type : string<br> Min. length :  - <br> Max. length :  - <br> Regex :  - <br>  |
| >Division Id | &#xA;&#xA;The division ID of the box belongs to.&#xA; |  -  | No | No | No | No |  -  | Data Type : string<br> Min. length :  - <br> Max. length :  - <br> Regex :  - <br> Allow Null : true<br>  |
| >Department Id | &#xA;&#xA;Department id belongs to the division selected, else department is null.&#xA; |  -  | No | No | No | No |  -  | Data Type : string<br> Min. length :  - <br> Max. length :  - <br> Regex :  - <br> Allow Null : true<br>  |
| >Major Description | &#xA;&#xA;Major description about the box contents.&#xA; | RMI TEST BOX | No | No | No | No |  -  | Data Type : string<br> Min. length :  - <br> Max. length :  - <br> Regex :  - <br> Allow Null : true<br>  |
| >Minor Description | &#xA;&#xA;Minor description about the box contents.&#xA; |  -  | No | No | No | No |  -  | Data Type : string<br> Min. length :  - <br> Max. length :  - <br> Regex :  - <br> Allow Null : true<br>  |
| >Long Description | &#xA;&#xA;Long description about the Iron Mountain box.&#xA; | QWE QWE WERWE | No | No | No | No |  -  | Data Type : string<br> Min. length :  - <br> Max. length :  - <br> Regex :  - <br> Allow Null : true<br>  |
| >From Date | &#xA;&#xA;Boxes/Files contents belongs to this from date of the customer. The format of the value is [yyyy/mm/dd].&#xA; | 2011/01/31 | No | No | No | No |  -  | Data Type : string<br> Min. length :  - <br> Max. length :  - <br> Regex : /([12]\d{3}\/(0[1-9]|1[0-2])\/(0[1-9]|[12]\d|3[01]))/<br> Allow Null : true<br>  |
| >To Date | &#xA;&#xA;Boxes/Files contents belongs to this To date of the customer.The format of the value is [yyyy/mm/dd].&#xA; | 2020/01/31 | No | No | No | No |  -  | Data Type : string<br> Min. length :  - <br> Max. length :  - <br> Regex : /([12]\d{3}/(0[1-9]|1[0-2])/(0[1-9]|[12]\d|3[01]))/<br>  |
| >Create Date | &#xA;&#xA;The create date of the box.The format of the value is [yyyy/mm/dd].&#xA; | 2011/05/17 | No | No | No | No |  -  | Data Type : string<br> Min. length :  - <br> Max. length :  - <br> Regex : /([12]\d{3}/(0[1-9]|1[0-2])/(0[1-9]|[12]\d|3[01]))/<br>  |
| >Receipt Date | &#xA;&#xA;The receipt date of the box.The format of the value is [yyyy/mm/dd].&#xA; | 2012/06/19 | No | No | No | No |  -  | Data Type : string<br> Min. length :  - <br> Max. length :  - <br> Regex : /([12]\d{3}/(0[1-9]|1[0-2])/(0[1-9]|[12]\d|3[01]))/<br>  |
| >Alpha From | &#xA;&#xA;The alpha from value given for the box.&#xA; |  -  | No | No | No | No |  -  | Data Type : string<br> Min. length :  - <br> Max. length :  - <br> Regex :  - <br> Allow Null : true<br>  |
| >Alpha To | &#xA;&#xA;The alpha to value given for the box.&#xA; |  -  | No | No | No | No |  -  | Data Type : string<br> Min. length :  - <br> Max. length :  - <br> Regex :  - <br> Allow Null : true<br>  |
| >Destruction Date | &#xA;&#xA;The destruction date of the box. The format of the value is [yyyy/mm/dd].&#xA; | 2011/08/17 | No | No | No | No |  -  | Data Type : string<br> Min. length :  - <br> Max. length :  - <br> Regex : /([12]\d{3}/(0[1-9]|1[0-2])/(0[1-9]|[12]\d|3[01]))/<br> Allow Null : true<br>  |
| >Transaction DateTime | &#xA;&#xA;The last modified date/time of the box.&#xA; | 2014/01/22T01:24:33.000 | No | No | No | No |  -  | Data Type : string<br> Min. length :  - <br> Max. length :  - <br> Regex : /^\d{4}/\d\d/\d\dT\d\d:\d\d:\d\d(\.\d&#x2B;)?(([&#x2B;-]\d\d:\d\d)|Z)?$/<br>  |
| >Transaction UserId | &#xA;&#xA;The user who did the last modification to the box.&#xA; | bob | No | No | No | No |  -  | Data Type : string<br> Min. length :  - <br> Max. length :  - <br> Regex :  - <br>  |
| >Special FileFormat | &#xA;&#xA;The special file format value. e.g FIL - file format&#xA; | FIL | No | No | No | No | FIL | Data Type : string<br> Min. length :  - <br> Max. length :  - <br> Regex :  - <br> Enum : FIL<br>  |
| >File GroupId | &#xA;&#xA;The file group ID assigned to the box.&#xA; | 0 | No | No | No | No |  -  | Data Type : string<br> Min. length :  - <br> Max. length :  - <br> Regex :  - <br> Allow Null : true<br>  |
| >Reference Number1 |  |  -  | No | No | No | No |  -  | Data Type : string<br> Min. length :  - <br> Max. length :  - <br> Regex :  - <br>  |
| >Reference Number2 |  |  -  | No | No | No | No |  -  | Data Type : string<br> Min. length :  - <br> Max. length :  - <br> Regex :  - <br>  |
| >Record Code |  |  -  | No | No | No | No |  -  | Data Type : string<br> Min. length :  - <br> Max. length :  - <br> Regex :  - <br>  |
| >Destruction Indicator |  |  -  | No | No | No | No |  -  | Data Type : string<br> Min. length :  - <br> Max. length :  - <br> Regex :  - <br>  |
| >Transmission Record |  | True | No | No | No | No |  -  | Data Type : boolean<br>  |
| >Event Date |  | 2011/04/18 | No | No | No | No |  -  | Data Type : string<br> Min. length :  - <br> Max. length :  - <br> Regex :  - <br>  |
| >Customer Barcode |  |  -  | No | No | No | No |  -  | Data Type : string<br> Min. length :  - <br> Max. length :  - <br> Regex :  - <br>  |
| >Hold Code |  | HLD | No | No | No | No |  -  | Data Type : string<br> Min. length :  - <br> Max. length :  - <br> Regex :  - <br>  |
| >Box Type |  |  -  | No | No | No | No |  -  | Data Type : object<br>  |
| >>Box Type |  | 1.2 | No | No | No | No |  -  | Data Type : string<br> Min. length :  - <br> Max. length :  - <br> Regex :  - <br>  |
| >>Box Description |  | Standard 1.2 cubft box | No | No | No | No |  -  | Data Type : string<br> Min. length :  - <br> Max. length :  - <br> Regex :  - <br>  |
| >>Box Cubic Foot |  | 1.2 | No | No | No | No |  -  | Data Type : number<br> Minimum :  - <br> Exclusive Minimum : No<br> Maximum :  - <br> Exclusive Maximum : No<br> Multiple Of :  - <br> Allow Null : false<br> Enum : 1.2, 2.4<br>  |
| >Box Entry Id | &#xA;&#xA;A temporary box entry id, e.g refer tempInvRecordNumber with Iron Mountain.&#xA; | tempInv123 | No | No | No | No |  -  | Data Type : string<br> Min. length :  - <br> Max. length :  - <br> Regex :  - <br> Allow Null : false<br>  |



#### Json sample
```
{
  "customerBoxNumber": "2",
  "customerId": "X999",
  "divisionId": "",
  "departmentId": "",
  "majorDescription": "RMI TEST BOX",
  "minorDescription": "",
  "longDescription": "QWE QWE WERWE",
  "fromDate": "2011/01/31",
  "toDate": "2020/01/31",
  "createDate": "2011/05/17",
  "receiptDate": "2012/06/19",
  "alphaFrom": "",
  "alphaTo": "",
  "destructionDate": "2011/08/17",
  "transactionDateTime": "2014/01/22T01:24:33.000",
  "transactionUserId": "bob",
  "specialFileFormat": "FIL",
  "fileGroupId": "0",
  "referenceNumber1": "",
  "referenceNumber2": "",
  "recordCode": "",
  "destructionIndicator": "",
  "transmissionRecord": false,
  "eventDate": "2011/04/18",
  "customerBarcode": "",
  "holdCode": "HLD",
  "boxType": {
    "type": "1.2",
    "description": "Standard 1.2 cubft box",
    "cubicFoot": 1.2
  },
  "boxEntryId": "tempInv123"
}
```


#### Json Schema
```
{
  "$schema": "http://json-schema.org/draft-04/schema#",
  "title": "Box",
  "description": "<p>Contains all details of the box record.</p>",
  "type": "object",
  "properties": {
    "customerBoxNumber": {
      "title": "Customer Box Number",
      "description": "<p>The box number defined/used by the customer.</p>",
      "type": "string",
      "pattern": ""
    },
    "customerId": {
      "title": "Customer Id",
      "description": "<p>The customer ID of the customer the box belongs to.</p>",
      "type": "string",
      "pattern": ""
    },
    "divisionId": {
      "title": "Division Id",
      "description": "<p>The division ID of the box belongs to.</p>",
      "type": "string",
      "pattern": ""
    },
    "departmentId": {
      "title": "Department Id",
      "description": "<p>Department id belongs to the division selected, else department is null.</p>",
      "type": "string",
      "pattern": ""
    },
    "majorDescription": {
      "title": "Major Description",
      "description": "<p>Major description about the box contents.</p>",
      "type": "string",
      "pattern": ""
    },
    "minorDescription": {
      "title": "Minor Description",
      "description": "<p>Minor description about the box contents.</p>",
      "type": "string",
      "pattern": ""
    },
    "longDescription": {
      "title": "Long Description",
      "description": "<p>Long description about the Iron Mountain box.</p>",
      "type": "string",
      "pattern": ""
    },
    "fromDate": {
      "title": "From Date",
      "description": "<p>Boxes/Files contents belongs to this from date of the customer. The format of the value is [yyyy/mm/dd].</p>",
      "type": "string",
      "pattern": "/([12]\\d{3}\\/(0[1-9]|1[0-2])\\/(0[1-9]|[12]\\d|3[01]))/"
    },
    "toDate": {
      "title": "To Date",
      "description": "<p>Boxes/Files contents belongs to this To date of the customer.The format of the value is [yyyy/mm/dd].</p>",
      "type": "string",
      "pattern": "/([12]\\d{3}/(0[1-9]|1[0-2])/(0[1-9]|[12]\\d|3[01]))/"
    },
    "createDate": {
      "title": "Create Date",
      "description": "<p>The create date of the box.The format of the value is [yyyy/mm/dd].</p>",
      "type": "string",
      "pattern": "/([12]\\d{3}/(0[1-9]|1[0-2])/(0[1-9]|[12]\\d|3[01]))/"
    },
    "receiptDate": {
      "title": "Receipt Date",
      "description": "<p>The receipt date of the box.The format of the value is [yyyy/mm/dd].</p>",
      "type": "string",
      "pattern": "/([12]\\d{3}/(0[1-9]|1[0-2])/(0[1-9]|[12]\\d|3[01]))/"
    },
    "alphaFrom": {
      "title": "Alpha From",
      "description": "<p>The alpha from value given for the box.</p>",
      "type": "string",
      "pattern": ""
    },
    "alphaTo": {
      "title": "Alpha To",
      "description": "<p>The alpha to value given for the box.</p>",
      "type": "string",
      "pattern": ""
    },
    "destructionDate": {
      "title": "Destruction Date",
      "description": "<p>The destruction date of the box. The format of the value is [yyyy/mm/dd].</p>",
      "type": "string",
      "pattern": "/([12]\\d{3}/(0[1-9]|1[0-2])/(0[1-9]|[12]\\d|3[01]))/"
    },
    "transactionDateTime": {
      "title": "Transaction DateTime",
      "description": "<p>The last modified date/time of the box.</p>",
      "type": "string",
      "pattern": "/^\\d{4}/\\d\\d/\\d\\dT\\d\\d:\\d\\d:\\d\\d(\\.\\d+)?(([+-]\\d\\d:\\d\\d)|Z)?$/"
    },
    "transactionUserId": {
      "title": "Transaction UserId",
      "description": "<p>The user who did the last modification to the box.</p>",
      "type": "string",
      "pattern": ""
    },
    "specialFileFormat": {
      "title": "Special FileFormat",
      "description": "<p>The special file format value. e.g FIL - file format</p>",
      "type": "string",
      "default": "FIL",
      "pattern": "",
      "enum": [
        "FIL"
      ]
    },
    "fileGroupId": {
      "title": "File GroupId",
      "description": "<p>The file group ID assigned to the box.</p>",
      "type": "string",
      "pattern": ""
    },
    "referenceNumber1": {
      "title": "Reference Number1",
      "type": "string"
    },
    "referenceNumber2": {
      "title": "Reference Number2",
      "type": "string"
    },
    "recordCode": {
      "title": "Record Code",
      "type": "string"
    },
    "destructionIndicator": {
      "title": "Destruction Indicator",
      "type": "string"
    },
    "transmissionRecord": {
      "title": "Transmission Record",
      "type": "boolean"
    },
    "eventDate": {
      "title": "Event Date",
      "type": "string"
    },
    "customerBarcode": {
      "title": "Customer Barcode",
      "type": "string"
    },
    "holdCode": {
      "title": "Hold Code",
      "type": "string"
    },
    "boxType": {
      "title": "Box Type",
      "description": "",
      "type": "object",
      "properties": {
        "type": {
          "title": "Box Type",
          "type": "string"
        },
        "description": {
          "title": "Box Description",
          "type": "string"
        },
        "cubicFoot": {
          "title": "Box Cubic Foot",
          "description": "",
          "type": "number",
          "enum": [
            "1.2",
            "2.4"
          ]
        }
      }
    },
    "boxEntryId": {
      "title": "Box Entry Id",
      "description": "<p>A temporary box entry id, e.g refer tempInvRecordNumber with Iron Mountain.</p>",
      "type": "string",
      "pattern": ""
    }
  }
}
```

---