# Boxes

## Description

&#xA;&#xA;Manages boxes&#xA;

|                                       |                                                 |
| ------------------------------------- | ----------------------------------------------- |
| API                                   | Boxes                                           |
| Base Path                             | customers/{customerId}/boxes      |
| Audience                              | Internal (domain restricted access) - not discoverable to external developers                                          |
| *Environment* <br> *Version* | Development US <br> 1.0.0.102  |
| GIT Branch                            | -                                           |
| API Products                          | -                                           |
| Version History                       | -                                           |
| Product and Services                  | -                                           |
| Config                                | -                                           |