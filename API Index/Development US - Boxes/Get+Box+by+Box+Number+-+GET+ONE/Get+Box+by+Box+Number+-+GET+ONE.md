# Get Box by Box Number - GET ONE

GET /v1/customers/{customerId}/boxes

## Description

&#xA;&#xA;Get Box by Box Number&#xA;

* [Request Payloads](#request-payloads)
* [Response Payloads](#response-payloads)

| HTTP Method                           | Get|
| ------------------------------------- | ----------------------------------------------- |
| API                                   | Boxes                                           |
| Api Version                           | 1.0.0.102                                         |
| Resource Version                      | 1                                               |
| Summary                               |                                       |
| Base Path                             | /v1/customers/{customerId}/boxes                                     |
| Resource                              | Get Box by Box Number                                      |
| Endpoint URL                          | https://wsg-dev-use.apigwy-np.eapg.ironmountain.cloud/v1/customers/{customerId}/boxes              |
| Service Status                        |  -                                          |
| Legislative / Regulatory / Compliance |                                             |
| Firewalls Details                     |                                              |
| Security Certificate Details          |                                              |
| Vendor or Partner Considerations      |                                             |

## Request Payloads

### Request Header



| Header | Description | Sample | PII | Sensitive | Unique Identifier | Mandatory | Default | Details |
| ------ | :---------: | :----: | :-: | :-------: | :---------------: | :-------: | :-----: | ------- |
| Api Header |  |  -  | No | No | No | No |  -  | Data Type : object<br>  |


---

### Query Params



| Parameter | Description | Sample | PII | Sensitive | Unique Identifier | Mandatory | Default | Details |
| --------- | :---------: | :----: | :-: | :-------: | :---------------: | :-------: | :-----: | ------- |
| Api Query Parameter |  |  -  | No | No | No | No |  -  | Data Type : object<br>  |


---

### Request Body

#### Payload 



| Parameter | Description | Sample | PII | Sensitive | Unique Identifier | Mandatory | Default | Details |
| :----- | :-----: | :-----: | :-----: | :-----: | :-----: | :-----: | :-----: | :----- |
| Request Payload |  |  -  | No | No | No | No |  -  | Data Type : object<br>  |



#### Json sample
```
{}
```


#### Json Schema
```
{
  "title": "Request Payload",
  "type": "object"
}
```

---