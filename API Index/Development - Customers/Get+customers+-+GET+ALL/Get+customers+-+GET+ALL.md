# Get customers - GET ALL

GET /v1/customers

## Description



* [Request Payloads](#request-payloads)
* [Response Payloads](#response-payloads)

| HTTP Method                           | Get|
| ------------------------------------- | ----------------------------------------------- |
| API                                   | Customers                                           |
| Api Version                           | 1.0.0.8                                         |
| Resource Version                      | 1                                               |
| Summary                               |                                       |
| Base Path                             | /v1/customers                                     |
| Resource                              | Get customers                                      |
| Endpoint URL                          | http://api.flowstep.dev/v1/customers              |
| Service Status                        |  -                                          |
| Legislative / Regulatory / Compliance |                                             |
| Firewalls Details                     |                                              |
| Security Certificate Details          |                                              |
| Vendor or Partner Considerations      |                                             |

## Request Payloads

### Request Header



| Header | Description | Sample | PII | Sensitive | Unique Identifier | Mandatory | Default | Details |
| ------ | :---------: | :----: | :-: | :-------: | :---------------: | :-------: | :-----: | ------- |
| Api Header |  |  -  | No | No | No | No |  -  | Data Type : object<br>  |


---

### Query Params



| Parameter | Description | Sample | PII | Sensitive | Unique Identifier | Mandatory | Default | Details |
| --------- | :---------: | :----: | :-: | :-------: | :---------------: | :-------: | :-----: | ------- |
| Api Query Parameter |  |  -  | No | No | No | No |  -  | Data Type : object<br>  |
| >Limit |  | 20 | No | No | No | No |  -  | Data Type : integer<br> Minimum :  - <br> Exclusive Minimum : No<br> Maximum :  - <br> Exclusive Maximum : No<br> Multiple Of :  - <br>  |
| >Offset |  | 0 | No | No | No | No |  -  | Data Type : integer<br> Minimum :  - <br> Exclusive Minimum : No<br> Maximum :  - <br> Exclusive Maximum : No<br> Multiple Of :  - <br>  |


---

### Request Body

#### Payload 



| Parameter | Description | Sample | PII | Sensitive | Unique Identifier | Mandatory | Default | Details |
| :----- | :-----: | :-----: | :-----: | :-----: | :-----: | :-----: | :-----: | :----- |
| Request Payload |  |  -  | No | No | No | No |  -  | Data Type : object<br>  |



#### Json sample
```
{}
```


#### Json Schema
```
{
  "title": "Request Payload",
  "type": "object"
}
```

---

## Response Payloads

# 200

| Resource                              | Get customers |
| ------------------------------------- | ----------------------------------------------- |
| Path                                  |  |
| Error Type                            | [system] |
| Response ClientCode                   | 200 |
| Error ClientCode                      | 200.Get customers.[error code count index] |
| Error Message                         | [error message] |

## Meaning

-

## Response


#### Payload 



| Parameter | Description | Sample | PII | Sensitive | Unique Identifier | Mandatory | Default | Details |
| :----- | :-----: | :-----: | :-----: | :-----: | :-----: | :-----: | :-----: | :----- |
| Response Payload |  |  -  | No | No | No | No |  -  | Data Type : object<br>  |
| >Total |  | 50 | No | No | No | No |  -  | Data Type : integer<br> Minimum :  - <br> Exclusive Minimum : No<br> Maximum :  - <br> Exclusive Maximum : No<br> Multiple Of :  - <br>  |
| >Items |  |  -  | No | No | No | No |  -  | Data Type : array<br>  |
| >>Customer |  |  -  | No | No | No | No |  -  | Data Type : object<br>  |
| >>>Customer Id |  | 10 | No | No | No | No |  -  | Data Type : integer<br> Minimum :  - <br> Exclusive Minimum : No<br> Maximum :  - <br> Exclusive Maximum : No<br> Multiple Of :  - <br>  |
| >>>Customer name |  | abc | No | No | No | No |  -  | Data Type : string<br> Min. length :  - <br> Max. length :  - <br> Regex :  - <br>  |



#### Json sample
```
{
  "total": 50,
  "items": [
    {
      "customerId": 10,
      "customerName": "abc"
    }
  ]
}
```


#### Json Schema
```
{
  "title": "Response Payload",
  "type": "object",
  "properties": {
    "total": {
      "title": "Total",
      "type": "integer"
    },
    "items": {
      "title": "Items",
      "type": "array",
      "items": {
        "title": "Customer",
        "type": "object",
        "properties": {
          "customerId": {
            "title": "Customer Id",
            "type": "integer"
          },
          "customerName": {
            "title": "Customer name",
            "type": "string"
          }
        }
      }
    }
  }
}
```
