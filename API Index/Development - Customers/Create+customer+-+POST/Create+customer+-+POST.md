# Create customer - POST

POST /v1/customers

## Description



* [Request Payloads](#request-payloads)
* [Response Payloads](#response-payloads)

| HTTP Method                           | Post|
| ------------------------------------- | ----------------------------------------------- |
| API                                   | Customers                                           |
| Api Version                           | 1.0.0.8                                         |
| Resource Version                      | 1                                               |
| Summary                               |                                       |
| Base Path                             | /v1/customers                                     |
| Resource                              | Create customer                                      |
| Endpoint URL                          | http://api.flowstep.dev/v1/customers              |
| Service Status                        |  -                                          |
| Legislative / Regulatory / Compliance |                                             |
| Firewalls Details                     |                                              |
| Security Certificate Details          |                                              |
| Vendor or Partner Considerations      |                                             |

## Request Payloads

### Request Header



| Header | Description | Sample | PII | Sensitive | Unique Identifier | Mandatory | Default | Details |
| ------ | :---------: | :----: | :-: | :-------: | :---------------: | :-------: | :-----: | ------- |
| Api Header |  |  -  | No | No | No | No |  -  | Data Type : object<br>  |


---

### Query Params



| Parameter | Description | Sample | PII | Sensitive | Unique Identifier | Mandatory | Default | Details |
| --------- | :---------: | :----: | :-: | :-------: | :---------------: | :-------: | :-----: | ------- |
| Api Query Parameter |  |  -  | No | No | No | No |  -  | Data Type : object<br>  |


---

### Request Body

#### Payload 



| Parameter | Description | Sample | PII | Sensitive | Unique Identifier | Mandatory | Default | Details |
| :----- | :-----: | :-----: | :-----: | :-----: | :-----: | :-----: | :-----: | :----- |
| Customer |  |  -  | No | No | No | No |  -  | Data Type : object<br>  |
| >Customer Id |  | 10 | No | No | No | No |  -  | Data Type : integer<br> Minimum :  - <br> Exclusive Minimum : No<br> Maximum :  - <br> Exclusive Maximum : No<br> Multiple Of :  - <br>  |
| >Customer name |  | abc | No | No | No | No |  -  | Data Type : string<br> Min. length :  - <br> Max. length :  - <br> Regex :  - <br>  |



#### Json sample
```
{
  "customerId": 10,
  "customerName": "abc"
}
```


#### Json Schema
```
{
  "$schema": "http://json-schema.org/draft-04/schema#",
  "title": "Customer",
  "type": "object",
  "properties": {
    "customerId": {
      "title": "Customer Id",
      "type": "integer"
    },
    "customerName": {
      "title": "Customer name",
      "type": "string"
    }
  }
}
```

---

## Response Payloads

# 201

| Resource                              | Create customer |
| ------------------------------------- | ----------------------------------------------- |
| Path                                  |  |
| Error Type                            | [system] |
| Response ClientCode                   | 201 |
| Error ClientCode                      | 201.Create customer.[error code count index] |
| Error Message                         | [error message] |

## Meaning

-

## Response


#### Payload 



| Parameter | Description | Sample | PII | Sensitive | Unique Identifier | Mandatory | Default | Details |
| :----- | :-----: | :-----: | :-----: | :-----: | :-----: | :-----: | :-----: | :----- |
| Customer |  |  -  | No | No | No | No |  -  | Data Type : object<br>  |
| >Customer Id |  | 10 | No | No | No | No |  -  | Data Type : integer<br> Minimum :  - <br> Exclusive Minimum : No<br> Maximum :  - <br> Exclusive Maximum : No<br> Multiple Of :  - <br>  |
| >Customer name |  | abc | No | No | No | No |  -  | Data Type : string<br> Min. length :  - <br> Max. length :  - <br> Regex :  - <br>  |



#### Json sample
```
{
  "customerId": 10,
  "customerName": "abc"
}
```


#### Json Schema
```
{
  "$schema": "http://json-schema.org/draft-04/schema#",
  "title": "Customer",
  "type": "object",
  "properties": {
    "customerId": {
      "title": "Customer Id",
      "type": "integer"
    },
    "customerName": {
      "title": "Customer name",
      "type": "string"
    }
  }
}
```

# 400

| Resource                              | Create customer |
| ------------------------------------- | ----------------------------------------------- |
| Path                                  |  |
| Error Type                            | [system] |
| Response ClientCode                   | 400 |
| Error ClientCode                      | 400.Create customer.[error code count index] |
| Error Message                         | [error message] |

## Meaning

The request is invalid, malformed or does not meet mandatory field requirements.

## Response


#### Payload 



| Parameter | Description | Sample | PII | Sensitive | Unique Identifier | Mandatory | Default | Details |
| :----- | :-----: | :-----: | :-----: | :-----: | :-----: | :-----: | :-----: | :----- |
| Bad Request - 400 |  |  -  | No | No | No | No |  -  | Data Type : object<br>  |
| >Bad Request Message |  | Generic / unknown error. Should also be delivered in case of input payload fails business logic validation | No | No | No | No |  -  | Data Type : string<br> Min. length :  - <br> Max. length :  - <br> Regex :  - <br>  |
| >Http Error Code |  | 400 | No | No | No | No |  -  | Data Type : integer<br> Minimum :  - <br> Exclusive Minimum : No<br> Maximum :  - <br> Exclusive Maximum : No<br> Multiple Of :  - <br>  |



#### Json sample
```
{
  "message": "Generic / unknown error. Should also be delivered in case of input payload fails business logic validation",
  "code": 400
}
```


#### Json Schema
```
{
  "$schema": "http://json-schema.org/draft-04/schema#",
  "title": "Bad Request - 400",
  "type": "object",
  "properties": {
    "message": {
      "title": "Bad Request Message",
      "type": "string"
    },
    "code": {
      "title": "Http Error Code",
      "type": "integer"
    }
  }
}
```
